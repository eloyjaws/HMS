Template.registerHelper('unassigned', function () {
    if (Meteor.user()) {
        var userId = Meteor.user()._id;
        var student = Student.findOne({userId: userId});
        //var matric_no = student.matric_no;
        //var assignment = Assignments.findOne({studentId: matric_no});
        if (!student) {
            return true;
        } else {
            return false;
        }
    }

});
Template.registerHelper('changeRequested', function () {
    if (Meteor.user()) {
        var userId = Meteor.user()._id;
        var student = Student.findOne({userId: userId});
        var matric_no = student.matric_no;
        var changeRequest = ChangeRequests.findOne({requested_by: matric_no, declined:false});
        if (!changeRequest) {
            return false;
        } else {
            return true;
        }
    }
});

Template.complaints.events({
    'submit #complaint_form': function (event) {
        event.preventDefault();
        var complaint = event.target.complaint.value;
        var category = event.target.category.value;
        var username = '';
        if (Meteor.user()) {
            username = Meteor.user().username;
        }
        var userId = Meteor.user()._id;
        var student = Student.findOne({userId: userId});
        console.log(student);
        Complaints.insert({
            category: category,
            complaint: complaint,
            logged_by: username,
            removed: false,
            gender: student.gender,
            roomNo: student.roomNo,
            blockNo: student.blockNo,
            createdAt: new Date()
        }, function (err) {
            if (err) {
                console.log("error");
                toastr.error('There was an error submitting the complaint');
            } else {
                toastr.success('Complaint submitted successfully!');
                Router.go('/');
            }
        });
    }
});

Template.changeRequest.events({
    'submit #change_req_form': function (event) {
        event.preventDefault();
        var reason = event.target.reason.value;

        var username = '';
        if (Meteor.user()) {
            username = Meteor.user().username;
        }
        
        var userId = Meteor.user()._id;
        var student = Student.findOne({userId: userId});

        ChangeRequests.insert({
            reason: reason,
            requested_by: username,
            declined: false,
            gender: student.gender,
            roomNo: student.roomNo,
            blockNo: student.blockNo,
            createdAt: new Date()
        }, function (err) {
            if (err) {
                console.log("error");
                toastr.error('There was an error submitting the request');
            } else {
                toastr.success('Request submitted successfully!');
                Router.go('/');
            }
        });
    }
});


Template.registerHelper('userId', function(){
    var userId = Meteor.user()._id;
    return userId;
});

Template.student.helpers({
    student: function(){
        var matNo = Meteor.user().username;
        var person = Student.findOne({matric_no: matNo});
        var room = Rooms.findOne({occupants: {$in:[matNo]}});
        
        var roomieNames = [];
        var occupants = room.occupants;
        for(var i=0; i<occupants.length;i++){
            var studentmatno = occupants[i];
            var student = Student.findOne({matric_no:studentmatno});
            roomieNames[i] = student.fullName;
        }

        var result = {
            roomNo: person.roomNo,
            blockNo: person.blockNo,
            vacancy: room.vacancy,
            occupant1: room.occupants[0],
            occupant2: room.occupants[1],
            occupant3: room.occupants[2],
            roomieNames1: roomieNames[0],
            roomieNames2: roomieNames[1],
            roomieNames3: roomieNames[2]
        }
        return result;
    }
});

Template.requests.helpers({
    allocationRequests: function(){
        var gender = Meteor.user().profile.gender;
        var requests = Assignments.find({gender: gender, verified:false}).fetch();
        return requests;
    }
});
