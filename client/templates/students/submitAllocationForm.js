Template.submitAllocationForm.events({
    'submit .allocationForm': function(event){
        event.preventDefault();
        var fullName = event.target.fullName.value;
        var gender = event.target.gender.value;
        var level = event.target.level.value;
        var department = event.target.department.value;

        var exec = $('#exec:checked').val();
        var exco = $('#exco:checked').val();
        var rehab = $('#rehab:checked').val();
        var wheel = $('#wheel:checked').val();
        var none = $('#none:checked').val();
        var matric_no;
        if (Meteor.user()){
            matric_no = Meteor.user().username;
        }
        var studentYear = "";
        var level = parseInt(level);
        if(level==000){
            studentYear = "foundation";
        } else if(level>=400){
            studentYear = "finalist";
        } else {
            studentYear = null;
        }
        var category = "none";
        var categories = [none, exec, exco, rehab, studentYear, wheel];
        
        for(var i=0; i<categories.length; i++){
            if(categories[i]){
                category = categories[i];
            }else{
                categories[i] = "";
            }
        }
        var rooms = Rooms.find({vacancy: {$gt: 0}, gender: gender, category: category},{limit:5, sort:{blockNo:1,roomNo:1}}).fetch();
        if(rooms.length === 0){
            console.log("no rooms found for special category");
            rooms = Rooms.find({vacancy: {$gt: 0}, gender: gender, category:null},{limit:5, sort:{blockNo:1,roomNo:1}}).fetch();
        }
        console.log(rooms);
        room = rooms[0];
        console.log(room);

        var student = Student.insert({
            fullName: fullName,
            userId: Meteor.user()._id,
            matric_no: matric_no,
            gender: gender,
            level: level,
            department: department,
            categories: categories,
            category: category,
            email: Meteor.user().emails[0].address,
            assigned: true,
        });

        var new_vacancy = parseInt(room.vacancy) - 1;
        Rooms.update({_id:room._id},{
            $set:{
                vacancy: new_vacancy 
            }
        });

        var hall = Halls.findOne({_id: room.hallId});

        Assignments.insert({
            studentId: matric_no,
            studentName: fullName,
            gender: gender,
            level: level,
            roomId: room._id,
            roomNo: room.roomNo,
            blockNo: room.blockNo,
            hallId: room.hallId,
            hallName: hall.hallName,
            category: category,
            verified: false,
            createdAt: new Date()
        });

        toastr.success("Application Submitted Successfully");
        Router.go("/");    
    }
});
