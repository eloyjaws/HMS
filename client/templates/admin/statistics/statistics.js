Template.statistics.helpers({
    halls: function(){
        return Halls.find().fetch();
    },
    vacancy: function(hallId){
        var roomspace = 0;
        roomspace += Rooms.find({hallId: hallId, vacancy:1}).count();
        roomspace += (Rooms.find({hallId: hallId, vacancy:2}).count() * 2);
        roomspace += (Rooms.find({hallId: hallId, vacancy:3}).count() * 3);
        return roomspace;
    }
});