Template.addHall.events({
    'submit #addHallForm': function(event){
        event.preventDefault();
        var hallName = event.target.hallName.value;
        var gender = event.target.gender.value;
        var warden = event.target.warden.value;
        var blocks = event.target.blocks.value;
        var rooms = event.target.rooms.value;
        var vacancy = event.target.vacancy.value;

        var hallCapacity = blocks*rooms*vacancy;

         var hall = Halls.insert({
            hallName: hallName,
            gender: gender,
            warden: warden,
            blocks: blocks,
            rooms: rooms,
            hallCapacity: hallCapacity,
            vacancy: vacancy
        });

        var currRoomId;
        for(var i=1; i<=blocks; i++){
            for(var j=1; j<=rooms; j++){
                currRoomId = Rooms.insert({
                    roomId: (((i-1)*rooms)+j),
                    hallId: hall,
                    blockNo: i,
                    roomNo: j,
                    vacancy: parseInt(vacancy),
                    occupants:[],
                    gender: gender
                });
                if((j===1)||(j===12)||(j===13)||(j===24)){
                    Rooms.update({_id:currRoomId},{
                        $push:{
                            category: 'finalist'
                        }
                    });
                }
            }
        }
        Router.go('/editHall/' + hall);
    }
});

Template.halls.events({
    'click #deleteHall': function(){
        event.preventDefault();
        hallId = this._id;
		if (confirm("Are You Sure?")) {
            rooms = Rooms.find({hallId: hallId}).fetch();
             _.each(rooms, function(room){
                roomId = room._id;
                Rooms.remove(roomId);
		    });
			Halls.remove(hallId);
			toastr.success("Hall deleted successfully");
			return false;
		}
	}
});

Template.addSpecialBlock.events({
    'submit #addSpecialBlockForm': function(event){
        event.preventDefault();
        var category = event.target.category.value;
        var blockNo = event.target.blockNo.value;
        var hallId = event.target.hallId.value;

        //write category to rooms with specified blockNo'
        rooms = Rooms.find({hallId: hallId, blockNo: parseInt(blockNo)}).fetch();
             _.each(rooms, function(room){  
                var j = room.roomNo;
                if((j===1)||(j===12)||(j===13)||(j===24)){

                } else {
                    roomId = room._id;
                    Rooms.update({_id:roomId},{
                        $push:{
                            category: category
                        }
                    });
                }
		    });
			toastr.success("Category added successfully");

    }
});

Template.registerHelper('selected', function(gender1, gender2){
	if(gender1 == gender2){
		return 'selected';
	}
});

Template.registerHelper('genderString', function(gender){
	if(gender == 'M'){
		return 'Male';
	}
    if(gender == 'F'){
		return 'Female';
	}
});

