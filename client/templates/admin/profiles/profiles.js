 Template.addPorter.events({
	"submit .addPorterForm": function(event){
        event.preventDefault();
        var login_id = event.target.login_id.value;
		var email = event.target.email.value;
        var fullname = event.target.porterName.value;
		var gender = event.target.gender.value;
		var password = event.target.password.value;
		var password2 = event.target.password2.value;

		if (isNotEmpty(login_id) && isNotEmpty(email) && isNotEmpty(fullname) && isNotEmpty(password) && isEmail(email) && areValidPasswords(password, password2)) {
		var user = {
				email: email,
				password: password,
                username: login_id,
				profile: {
                    fullname: fullname,
					roles: ['porter'],
					gender: gender
				},
			}
			id = Accounts.createUser({

				email: user.email,
				password: user.password,
				username: user.username,
				profile: user.profile

			}, function(err){
				if (err) {
					console.log("error");
					toastr.error('There was an error with Porter registration');
				} else {
                    event.preventDefault();
					toastr.success('Porter profile has been created successfully!');
					Router.go('/');
				}
			});
			
			}
			return false;
    }
 });

 Template.registerHelper('getEmail', function(email){
	 var email = email[0].address;
	 return email;
 });

 Template.editProfile.events({
	 'click #delete-user': function(event){
		 var userId = this._id;
		 console.log("User id is " + userId);
		 if(confirm("Are you sure?")){
		 	Accounts.users.remove(userId);
			toastr.info("User remove successfully");
		 }
	 }
 });