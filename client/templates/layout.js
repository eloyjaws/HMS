Template.vacancy.rendered = function () {
    //Init FooTable 
      // $('.footable').footable();
      $('table').footable();
};
Template.requests.rendered = function () {
    //Init FooTable
    $('.footable').footable();
};
Template.studentDetails.rendered = function () {
    //Init FooTable
    $('.footable').footable();
};
Template.editProfile.rendered = function () {
    //Init FooTable
    $('.footable').footable();
};
Template.layout.rendered = function(){
    //scroll back to top
    document.body.scrollTop = document.documentElement.scrollTop = 0;
}
Template.loading.rendered = function () {
  if ( ! Session.get('loadingSplash') ) {
    this.loading = window.pleaseWait({
      logo: '/images/Meteor-logo.png',
      backgroundColor: '#7f8c8d',
      loadingHtml: message + spinner
    });
    Session.set('loadingSplash', true); // just show loading splash once
  }
};

Template.loading.destroyed = function () {
  if ( this.loading ) {
    this.loading.finish();
  }
};

var message = '<p class="loading-message">Glorious things are waiting for you. We are just getting them ready</p>';
var spinner = '<div class="sk-spinner sk-spinner-rotating-plane"></div>';


//profilecheck
Template.registerHelper('roleIs', function(role){

  if (Meteor.user()){
    if (Meteor.user().profile.roles[0] == role) {
		  return true;
	  } else {
      return false;
    }
  }
	
});

Template.registerHelper('user', function(){
  if (Meteor.user()){
    var username = Meteor.user().username;
		return username;
  }
});

toastr.options = {
  "closeButton": true,
  "debug": false,
  "newestOnTop": true,
  "progressBar": true,
  "positionClass": "toast-top-right",
  "preventDuplicates": false,
  "onclick": null,
  "showDuration": "300",
  "hideDuration": "1000",
  "timeOut": "5000",
  "extendedTimeOut": "1000",
  "showEasing": "swing",
  "hideEasing": "linear",
  "showMethod": "fadeIn",
  "hideMethod": "fadeOut"
};