Template.registerHelper('currentRoute', function(){
	var route = Router.current().route.getName();
    if (route == "home"){
        route = "";
    } 
    return route;
});

Template.registerHelper('getUsername', function(){
    if(Meteor.user()){
        var username = Meteor.user().username;
        return username;
    }
	
});

Template.navbar.events({
    "click .logout": function(event){
        Meteor.logout(function(err){
            if(err){
                toastr.error(err.reason);
            } else {
                toastr.success("You are Successfully logged out");
                Router.go('/');
            }
        });
    }
});