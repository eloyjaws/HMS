Template.login.rendered = function(){
    $('.register-panel').hide();
}
Template.login.events({
	"click .register": function(event){
        event.preventDefault();
		$('.login-panel').hide();
		$('.register-panel').fadeIn();
	},
	"click .login": function(event){
        event.preventDefault();
		$('.register-panel').hide();
		$('.login-panel').fadeIn();
	},
	"submit .register-form": function(event){
        event.preventDefault();
        var login_id = event.target.login_id.value;
		var email = event.target.email.value;
		var password = event.target.password.value;
		var password2 = event.target.password2.value;

		if (isNotEmpty(login_id) && isNotEmpty(email) && isNotEmpty(password) && isEmail(email) && areValidPasswords(password, password2)) {
		if (Meteor.users.find().count() < 1) {
			var user = {
					username: login_id,
					email: email,
					password: password,
					profile: {
						roles: ['admin']
					}
				}
		}
		else {
			if(isMatric(login_id)){
				var user = {
					email: email,
					password: password,
					username: login_id,
					profile: {
						roles: ['student'],
						allocated: 0
					},
				}
			} else {
				toastr.error("Matric number is invalid");
				Router.go('/');
			}
		}
		var id;
			id = Accounts.createUser({

				email: user.email,
				password: user.password,
				username: user.username,
				profile: user.profile

			}, function(err){
				if (err) {
					console.log("error");
					toastr.error('There was an error with registration');
				} else {
					id = Meteor.userId();
					toastr.success('You are now logged in');
					Router.go('/');
				}
			});
			
			}
			return false;
		},
		"submit .login-form": function(event){
            event.preventDefault();
			var login_id = event.target.login_id.value;
			var password = event.target.password.value;
			Meteor.loginWithPassword(login_id, password, function(error){
				if (error) {
					event.target.login_id.value = login_id;
					event.target.password.value = password;
					toastr.error(error.reason);
				} else {
					toastr.success('You are now logged in');
					Router.go('/');
				}
			});
				//clear form
				event.target.login_id.value = "";
				event.target.password.value = "";

				//prevent Submit
				return false;
		},

		
});


//validation rules

//trim helper
var trimInput = function(val){
	return val.replace(/^\s*|\s*$/g, "");
}

//check for empty fields
isNotEmpty = function(value){
	if (value && value !== '') {
		return true;
	}
	toastr.error('Please fill in all the fields');
	return false;
};

//validate email
isEmail = function(value){
	var filter = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (filter.test(value)) {
		return true;
	}
	toastr.error('Please use a valid Email address');
	return false;
};

//Check password field
isValidPassword = function(password){
	if (password.length < 6){
		toastr.error('Password must be at least 6 characters');
		return false;
	}
	return true;
};

//Match passwords
areValidPasswords = function(password, confirm){
	if (!isValidPassword(password)) {
		return false;
	}
	if (password !== confirm) {
		toastr.error('Passwords do not match!');
		return false;
	}
	return true;
};

//validate username as matric number
isMatric = function(value){
    var filter  = /^(R|r)(U|u)(N|n)(\/|\\)([a-zA-Z]){3}(\/|\\)([0-9]){2}(\/|\\)([0-9]){4}$/;
	if (filter.test(value)) {
		return true;
	}
	toastr.error('Please use a valid Matric number');
	return false;
};