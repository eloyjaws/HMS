
Template.studentDetails.helpers({
    student: function(){
        var gender = Meteor.user().profile.gender;
        var student = Student.find({gender: gender});
        return student;
    }
});

Template.vacancy.helpers({
    room: function(){
        var gender = Meteor.user().profile.gender;
        var rooms = Rooms.find({gender: gender},{sort:{blockNo:1, roomNo:1}});
        return rooms;
    }
});

Template.registerHelper('levelString', function(level){
	if(level === 0){
		return 'Foundation';
	}
    return level;
});
