Template.viewComplaints.helpers({
    carpentryComplaint: function(){
        var gender = Meteor.user().profile.gender;
        var complaints = Complaints.find({gender: gender, removed:false, category:"carpentry"}).fetch();
        return complaints;
    },
    electricalComplaint: function(){
        var gender = Meteor.user().profile.gender;
        var complaints = Complaints.find({gender: gender, removed:false, category:"electrical"}).fetch();
        return complaints;
    },
    plumbingComplaint: function(){
        var gender = Meteor.user().profile.gender;
        var complaints = Complaints.find({gender: gender, removed:false, category:"plumbing"}).fetch();
        return complaints;
    },
    otherComplaint: function(){
        var gender = Meteor.user().profile.gender;
        var complaints = Complaints.find({gender: gender, removed:false, category:"other"}).fetch();
        return complaints;
    },
    complaints: function(){
        var gender = Meteor.user().profile.gender;
        var complaint = Complaints.findOne({gender: gender, removed:false});
        if(!complaint){
            toastr.info("There are currently no complaints");
        }
        return complaint;
    },
    getComplaint: function(studentComplaint){
        var complaint = studentComplaint;
        console.log("Complaint = " + complaint);
        return complaint;
    }
});

Template.viewComplaints.events({
    'click #remove': function(event){
        event.preventDefault();
        console.log("Complaint id is " + this._id);
        var cId = this._id;
        if (confirm("Are you sure?")) {
			Complaints.update({_id:cId}, {
                $set:{
                    removed: true
                }
            });
			toastr.info("Complaint Removed Successfully");
		}
    }
});

//Format Date
Template.registerHelper('formatDate', function(date){
	return moment(date).format('DD-MM-YYYY');
});
