Template.requests.helpers({
    allocationRequests: function(){
        var gender = Meteor.user().profile.gender;
        var requests = Assignments.find({gender: gender, verified:false}).fetch();
        return requests;
    }
});

Template.requests.helpers({
    changeRequests: function(){
        var gender = Meteor.user().profile.gender;
        var requests = ChangeRequests.find({gender: gender, declined:false}).fetch();
        return requests;
    }
});

Template.requests.events({
    'click #validate-room': function(event){
        event.preventDefault();
        console.log("Assignment id is " + this._id);
        var assId = this._id;
        var assignment = Assignments.findOne(assId);
        var studentId = assignment.studentId;
        var student = Student.findOne({matric_no:studentId});
        //update verified to true
        Assignments.update({_id:assId},{
            $set:{
                verified: true 
            }
        });

        var studentUserId = student._id;
        Student.update({_id:studentUserId},{
            $set:{
                blockNo: assignment.blockNo,
                roomNo: assignment.roomNo
            }
        });
        var room = Rooms.findOne(assignment.roomId);
        var new_vacancy = parseInt(room.vacancy) - 1;
        Rooms.update({_id:assignment.roomId},{
            $push:{
                occupants: studentId
            },
            $set:{
                vacancy: new_vacancy
            }

        });
        // Client: Asynchronously send an email.
        Meteor.call(
        'sendEmail',
        'Alice <eloyjaws@gmail.com>',
        'meteormail@mail.com',
        'Hello from Meteor!',
        'This is a test of Email.send.'
        );
        
    },
    'click #reassign': function(event){
        event.preventDefault();
        console.log("Assignment id is " + this._id);
        var assId = this._id;
        var assignment = Assignments.findOne({_id:assId});

        var studentId = assignment.studentId;
        var student = Student.findOne({matric_no:studentId});
        console.log("Student is " + student + student.fullName);

        var prev_room = Rooms.findOne({_id:assignment.roomId});
        var prev_room_vacancy = prev_room.vacancy;
        var hall = Halls.findOne(assignment.hallId);
        var room_cap = hall.vacancy;
        var new_room_vacancy;
        if(prev_room_vacancy<room_cap){
            new_room_vacancy = prev_room_vacancy + 1;
        }else if (prev_room_vacancy>room_cap){
            new_room_vacancy = room_cap;
        }
        Rooms.update({_id:assignment.roomId},{
            $set:{
                vacancy: new_room_vacancy
            }
        });

        var rooms = Rooms.find({vacancy: {$gt: 0}, gender: student.gender, category: student.category},{limit:5, sort:{blockNo:1,roomNo:1}}).fetch();
        if(rooms.length === 0){
            console.log("no rooms found for special category");
            // rooms = Rooms.find({vacancy: {$gt: '0'}, gender: gender},{limit:5}).fetch();
            rooms = Rooms.find({vacancy: {$gt: 0}, gender: student.gender, category:null},{limit:5, sort:{blockNo:1,roomNo:1}}).fetch();
        }
        console.log(rooms);

        var rand = Math.random();
        rand = parseInt((rand*10)%5);
        
        room = rooms[rand];

        Assignments.update({_id:assId},{
            $set:{
                roomId: room._id,
                roomNo: room.roomNo,
                blockNo: room.blockNo,
                hallId: room.hallId,
                createdAt: new Date()
            }
        });

        toastr.success("Assignment changed Successfully");
    },
    'click #reallocate-room': function(event){
        event.preventDefault();
        console.log("ChangeRequest id is " + this._id);
        var crId = this._id;
        var cr = ChangeRequests.findOne(crId);
        var matNo = cr.requested_by;
        console.log("Matric No is" + matNo);
        var student = Student.findOne({matric_no: matNo});
        var assignment = Assignments.findOne({studentId: matNo});



        var prev_room = Rooms.findOne({_id:assignment.roomId});
        var prev_room_vacancy = prev_room.vacancy;
        var prev_room_occupants = prev_room.occupants;
        var i = 0;
        var new_occupants = [];
        for(var j=0; j<=prev_room_occupants.length-1; j++){
            if(prev_room_occupants[j]!==matNo){
                new_occupants[i] = prev_room_occupants[j];
            }
        }
        

        var hall = Halls.findOne(assignment.hallId);
        var room_cap = hall.vacancy;
        var new_room_vacancy;
        if(prev_room_vacancy<room_cap){
            new_room_vacancy = prev_room_vacancy + 1;
        }else if (prev_room_vacancy>room_cap){
            new_room_vacancy = room_cap;
        }

        Rooms.update({_id:assignment.roomId},{
            $set:{
                vacancy: new_room_vacancy,
                occupants:  new_occupants
            }
        });

        var rooms = Rooms.find({vacancy: {$gt: 0}, gender: student.gender, category: student.category},{limit:5, sort:{blockNo:1,roomNo:1}}).fetch();
        if(rooms.length === 0){
            console.log("no rooms found for special category");
            // rooms = Rooms.find({vacancy: {$gt: '0'}, gender: gender},{limit:5}).fetch();
            rooms = Rooms.find({vacancy: {$gt: 0}, gender: student.gender, category:null},{limit:5, sort:{blockNo:1,roomNo:1}}).fetch();
        }
        console.log(rooms);

        var rand = Math.random();
        rand = parseInt((rand*10)%5);
        
        room = rooms[rand];

        Assignments.update({_id:assignment._id},{
            $set:{
                roomId: room._id,
                roomNo: room.roomNo,
                blockNo: room.blockNo,
                hallId: room.hallId,
                verified: false,
                createdAt: new Date()
            }
        });

        ChangeRequests.remove(crId);

        toastr.info("Room changed Successfully and new assignment request Submitted!");

    },
    'click #decline-request': function(event){
        event.preventDefault();
        console.log("ChangeRequest id is " + this._id);
        var crId = this._id;
        if (confirm("Are you sure?")) {
            ChangeRequests.update({_id:crId},{
                $set:{
                    declined: true
                }
            });
			toastr.info("Room Change Request declined Successfully");
		}
    }
});