Router.configure({
	layoutTemplate: 'layout',
	loadingTemplate: "loading",
    notFoundTemplate: "404"
});

var OnBeforeActions = {
	isAdmin: function(){
		//if logged in
		if (Meteor.user()) {
			//if admin
			if (Meteor.user().profile.roles[0] != 'admin') {
				Router.go('/');
			} else {
				this.next();
			}
		} else {
			Router.go('/');
		}
	},
	isPorter: function(){
		//if logged in
		if (Meteor.user()) {
			//if porter
			if (Meteor.user().profile.roles[0] != 'porter') {
				Router.go('/');
			} else {
				this.next();
			}
		} else {
			Router.go('/');
		}
	},
	isStudent: function(){
		//if logged in
		if (Meteor.user()) {
			//if student
			if (Meteor.user().profile.roles[0] != 'student') {
				Router.go('/');
			} else {
				this.next();
			}
		} else {
			Router.go('/');
		}
	}
}


/*Router.onBeforeAction(OnBeforeActions.isPorter, {
	only: ['studentDetails', 'allocate', 'change', 'requests', 'studentDetails', 'deallocate', 'vacancy']
});	

Router.onBeforeAction(OnBeforeActions.isAdmin, {
	only: ['addHall', 'halls', 'addPorter', 'editProfile', 'statistics']
});	
Router.onBeforeAction(OnBeforeActions.isStudent, {
	only: []
});	*/

Router.map(function(){

	this.route('home',{
		path: '/',
		template: 'home',
		data: function(){
		}
	});

    //admin routes
    this.route('studentDetails');

	this.route('addHall');

	this.route('halls', {
		path: '/halls',
		template: 'halls',
		data: function(){
			templateData = {
				halls: Halls.find({})
			};
			return templateData;
		}
	});

	this.route('editHall', {
		path: '/editHall/:_id',
		template: 'editHall',
		data: function(){
			var currentHall = this.params._id;
			return Halls.findOne({_id: currentHall})
			
		}
	});

	this.route('addSpecialBlock', {
		path: '/addSpecialBlock/:_id',
		template: 'addSpecialBlock',
		data: function(){
			var currentHall = this.params._id;
			return Halls.findOne({_id: currentHall})
			
		}
	});

	this.route('addPorter');

	this.route('editProfile', {
		path: '/editProfile',
		template: 'editProfile',
		data: function(){
			templateData = {
				accounts: Accounts.users.find()
			};
			return templateData;
		}
		});

	this.route('statistics');

    this.route('allocate', {
		path: '/allocate',
		template: 'allocate'
	});

	this.route('change', {
		path: '/change',
		template: 'change'
	});

	this.route('requests', {
		path: '/requests',
		template: 'requests',
		data: function(){
			templateData = {
			}
		}
	});

	this.route('viewComplaints');

	this.route('student details', {
		path: '/student-details',
		template: 'studentDetails',
		data: function(){
			templateData = {
			}
		}
	});

    this.route('deallocate');

    this.route('vacancy');
    

    //student routes
    this.route('student', {
		path: '/student/:id',
		template: 'student',
		data: function(){
			var userId = this.params.id;
			var templateData = {
				student: Student.findOne({userId: userId})
			}
			return templateData;
		}
	});

	this.route('complaints');

	this.route('changeRequest');

	this.route('submitAllocationForm');
});